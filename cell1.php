<?php

// Cell contents for week 1

// For debugging
//echo '<font size="1px">Cell #' . $dayperiod . '</font>';

if ($_GET["room"] !== "laptops") {
    if (file_exists($ttdir1.$_GET['room'].'/'.$dayperiod.$ext)) {
        echo '<div id="timetabled">';
        include_once($ttdir1.$_GET['room'].'/'.$dayperiod.$ext);
        echo '</div>';
    }

    elseif (file_exists($bookdir1.$_GET['room'].'/'.$file.$dayperiod.$ext)) {
        include_once($bookdir1.$_GET['room'].'/'.$file.$dayperiod.$ext);
            if ((($_COOKIE["user"]) == $owner) OR $adminusers) {
                    echo '<input type="checkbox" name="del1[]" value="' 
                        .$file.$dayperiod.'" />';
            }
    }
    else {
        echo '<input type="checkbox" name="add1[]" value="'
            .$file.$dayperiod.'" />';
    }
}

// Laptops (multiple booking resource)
else {
    if (file_exists($ttdir1.$_GET['room'].'/'.$dayperiod.$ext)) {
        echo '<div id="timetabled">';
        include_once($ttdir1.$_GET['room'].'/'.$dayperiod.$ext);
        echo '</div>';
    }

    if ((file_exists($bookdir1.$_GET['room'].'/'.$file.$dayperiod.$ext)) 
        OR (file_exists($bookdir1.$_GET['room'].'/'.$file.$dayperiod.'-1'.$ext)) 
        OR (file_exists($bookdir1.$_GET['room'].'/'.$file.$dayperiod.'-2'.$ext))
        OR (file_exists($bookdir1.$_GET['room'].'/'.$file.$dayperiod.'-3'.$ext))) {
            if (file_exists($bookdir1.$_GET['room'].'/'.$file.$dayperiod.$ext)) {
                include_once($bookdir1.$_GET['room'].'/'.$file.$dayperiod.$ext);
                echo ' ' . $number;
                    if ((($_COOKIE["user"]) == $owner) OR $adminusers) {
                            echo '<input type="checkbox" name="del1[]" value="'
                                .$file.$dayperiod.'" />';
                    }
            }
    }

    if ((file_exists($bookdir1.$_GET['room'].'/'.$file.$dayperiod.'-1'.$ext))) {
        if (file_exists($bookdir1.$_GET['room'].'/'.$file.$dayperiod.$ext)) {
            echo '<br>';
            include_once($bookdir1.$_GET['room'].'/'.$file.$dayperiod.'-1'.$ext);
        }

        echo ' ' . $number;

        if ((($_COOKIE["user"]) == $owner) OR $adminusers) {
            echo '<input type="checkbox" name="del1[]" value="'
                .$file.$dayperiod.'-1" />';
        }
    }
    
    if (file_exists($bookdir1.$_GET['room'].'/'.$file.$dayperiod.'-2'.$ext)) {
        if ((file_exists($bookdir1.$_GET['room'].'/'.$file.$dayperiod.'-1'.$ext)) 
            OR (file_exists($bookdir1.$_GET['room'].'/'.$file.$dayperiod.$ext))) {
                echo '<br>';
                include_once($bookdir1.$_GET['room'].'/'.$file.$dayperiod.'-2'.$ext);
        }

        echo ' ' . $number;

        if ((($_COOKIE["user"]) == $owner) OR $adminusers) {
            echo '<input type="checkbox" name="del1[]" value="'
                .$file.$dayperiod.'-2" />';
        }
    }

    if ((file_exists($bookdir1.$_GET['room'].'/'.$file.$dayperiod.'-3'.$ext))) {
        if ((file_exists($bookdir1.$_GET['room'].'/'.$file.$dayperiod.'-2'.$ext)) 
            OR (file_exists($bookdir1.$_GET['room'].'/'.$file.$dayperiod.'-1'.$ext)) 
            OR (file_exists($bookdir1.$_GET['room'].'/'.$file.$dayperiod.$ext))) {
                echo '<br>';
                include_once($bookdir1.$_GET['room'].'/'.$file.$dayperiod.'-3'.$ext);
        }

        echo ' ' . $number;

        if ((($_COOKIE["user"]) == $owner) OR $adminusers) {
            echo '<input type="checkbox" name="del1[]" value="'
                .$file.$dayperiod.'-3" />';
        }
    }

    if ((!file_exists($bookdir1.$_GET['room'].'/'.$file.$dayperiod.$ext))
        OR (!file_exists($bookdir1.$_GET['room'].'/'.$file.$dayperiod.'-1'.$ext))
        OR (!file_exists($bookdir1.$_GET['room'].'/'.$file.$dayperiod.'-2'.$ext))
        OR (!file_exists($bookdir1.$_GET['room'].'/'.$file.$dayperiod.'-3'.$ext))) {
            echo '<br><input type="checkbox" name="add1[]" value="'
                .$file.$dayperiod.'" />';
    }

    else
        echo '<input type="checkbox" name="add1[]" value="'.$file.$dayperiod.'" />';
}
