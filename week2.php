<?php

echo '  <h1>Week 2</h1>

    <table class=main border=1>
        <tr>
            <td class="title">&nbsp;</td>';

    //Top row (period names)
    for ($celltitle2=1; $celltitle2<=$period; $celltitle2++) {
        $numtitle2++;

        if (($numtitle2 == 3) OR ($numtitle2 == 6) OR ($numtitle2 == 8))
            echo '<td class="titlefaded">';
        else
            echo '<td class="title">';

        echo $title[$numtitle2];
        echo '</td>';
    }

    echo '</tr>';

    //Left column (days)
    for ($cellday2=1; $cellday2<=$viewdays; $cellday2++) {
        $periodnum2=1;
        $numday2++;
        echo '<tr>
            <td class="day">'.$day[$numday2].'</td>';

        //Table cells
        for ($cell=1; $cell<=$period; $cell++) {
            $numcell2++;
            $dayperiod2=$numday2.'-'.$periodnum2;
            if (($periodnum2 == 3) OR ($periodnum2 == 6) OR ($periodnum2 == 8))
                echo '<td class="cellfaded">';
            else {
                echo '<td class="cell">';
                include("cell2.php");
            }
                echo '</td>';
                $periodnum2++;
        }
    }

    echo '</tr></table>';
