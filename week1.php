<?php

echo '  <h1>Week 1</h1>

    <table class=main border=1>
        <tr>
            <td class="title">&nbsp;</td>';

    //Top row (period names)
    for ($celltitle=1; $celltitle<=$period; $celltitle++) {
        $numtitle++;

        if (($numtitle == 3) OR ($numtitle == 6) OR ($numtitle == 8))
            echo '<td class="titlefaded">';
        else
            echo '<td class="title">';

        echo $title[$numtitle];
        echo '</td>';
    }

    echo '</tr>';

    //Left column (days)
    for ($cellday=1; $cellday<=$viewdays; $cellday++) {
        $periodnum1=1;
        $numday++;
        echo '<tr>
            <td class="day">'.$day[$numday].'</td>';

        //Table cells
        for ($cell=1; $cell<=$period; $cell++) {
            $numcell1++;
            $dayperiod=$numday.'-'.$periodnum1;
            if (($periodnum1 == 3) OR ($periodnum1 == 6) OR ($periodnum1 == 8))
                echo '<td class="cellfaded">';
            else {
                echo '<td class="cell">';
                include("cell1.php");
            }
                echo '</td>';
                $periodnum1++;
        }
    }

    echo '</tr></table>';
