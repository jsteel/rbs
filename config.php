<?php

// Note: the system is not dynamic when changing the number of periods and days;
// it will be OK to add to them, but if you reduce the number some bookings may
// become hidden

// How many periods to show (columns)
$period=7;
// Names of each period
$title[1]='P1<br><div id="time">08:55-09:55</div>';
$title[2]='P2<br><div id="time">09:55-10:55</div>';
$title[3]='Break<br><div id="time">10:55-11:15</div>';
$title[4]='P3<br><div id="time">11:15-12:15</div>';
$title[5]='P4<br><div id="time">12:15-13:15</div>';
$title[6]='Lunch<br><div id="time">13:15-14:00</div>';
$title[7]='P5<br><div id="time">14:00-15:00</div>';

// Days to show (rows)
$viewdays=5;
// Days
$day[1]="Monday";
$day[2]="Tuesday";
$day[3]="Wednesday";
$day[4]="Thursday";
$day[5]="Friday";
$day[6]="Saturday";
$day[7]="Sunday";

// Number of rooms to show (hyperlinks are created)
$roomnum="10";
// Rooms (lowercase and no special characters/spaces)
$room[1]="a4";
$room[2]="a6";
$room[3]="i1";
$room[4]="i2";
$room[5]="i3";
$room[6]="i5";
$room[7]="g2";
$room[8]="classroom";
$room[9]="computers";
$room[10]="laptops";

// Force a particular resource? (if just one is used) yes/no
$forceresource='no';
// Only needed if $forceresource='yes'
$resource='laptops';

// Directory where the booking files will be written to
$bookdir1='bookings/week1/';
$bookdir2='bookings/week2/';
// Directory where the timetable files will be kept
$ttdir1='timetable/week1/';
$ttdir2='timetable/week2/';
// File extension that the booking and timetable files will use
$ext='.txt';
// What the booking file names will begin with
$file='booking-';

// Title
$name="Resource Booking System";

// Optional welcome message 
$welcome="";

// Your logo
$logo="/images/logo.png";

// Admin users
$adminusers='($_COOKIE["user"] == jsteel) OR ($_COOKIE["user"] == admin)';

// Howto message
$howtomsg='<h3>Howto</h3>
<ol>
<li>Click the periods you wish to book or delete</li>
<li>Enter a comment and number required (if applicable) - not required for 
    deleting</li>
<li>Click Save Changes</li>
</ol>';

// Comment message
$commentmsg='<p class="comment">Short Comment <font size=1px color=red>*required
for booking</font> <font size=1px>(for example your class or department; your
name will be displayed automatically)</font></p>';
// Maximum length of a comment
$commentmax='10';

// Number required message
$numbermsg='<p>Number Required (30 max) <font size=1px color=red>*required for
booking</font> <font size=1px>(please check existing bookings to avoid
overbooking)</font></p>';
// Maximum length of the number required (2 will allow up to 99)
$numbermax='2';

// Saved message
$savedmsg='<h3>Saved!</h3>
<p>It seems as though everything went well, but you are encouraged to check that
you are happy with your booking below.';

// Duplicate booking error
$duplicate='<h3>Error</h3>
<p>Sorry but it looks like someone beat you to it and one (or more) of the
requested periods have just been booked by someone else.</p>
<p>Note that any other requests may have gone through.</p>
<p>Click the button below to go back and see what happened.</p>
<form action="index.php?room=' . $_POST['room'] . '" method="POST">
    <input type="submit" value="Go Back" />
</form>';

// Invalid resource error
$invalidmsg = '<p>Sorry but this is <b>not</b> a valid resource. Please try
selecting a resource listed above.</p>';

// Footer
$footer = '<a class="foot" href="http://bitbucket.org/jsteel/rbs">rbs</a>';

// LDAP (Active Directory) authentication
// IP address of a domain controller
$host = ""; 
// This deftault port should be fine
$port = "389";
// OU to search for users (limit to teachers?)
$base = "ou=3rdlevel,ou=2ndlevel,ou=1stlevel,dc=domain,dc=local";
// Log in error
$loginerror = '<font color="red">Wrong username and/or password</font>';
// Log out message
$logoutmsg='<font color="red">You have logged out</font>';
