<?php

ob_start();
if (!isset($_COOKIE["user"]))
    header('Location: login.php');

include_once('config.php');

if (($forceresource == 'yes') AND ($_GET["room"] != $resource))
    header('Location: index.php?room=' . $resource);

echo '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="style.css">
        <title>' . $name . '</title>
    </head>
<body>
    <table class="page" border=0>
        <tr>
            <td>
                <table class="header" border=0>
                    <tr>
                        <td class="logo">
                            <img src="' . $logo . '" alt="Logo">
                        </td>
                        <td rowspan="2" class="message">';

if (isset($_POST['save'])) {
    // This will increment if there are any problems
    $error=0;
    // If there is more than 1 duplicate bookings, the error will just be
    // displayed once
    $duplicatenum=0;

    // Get the current room
    $roomdir1=$bookdir1.$_POST['room'];
    $roomdir2=$bookdir2.$_POST['room'];

    // Check that the user entered some data and give appropriate errors if not
    if (((!count($_POST['add1']) > 0) AND (!count($_POST['add2']) > 0)
        AND (!count($_POST['del1']) > 0) AND (!count($_POST['del2']) > 0))
        OR ((count($_POST['add1']) > 0) AND (empty($_POST["comment"])))
        OR ((count($_POST['add2']) > 0) AND (empty($_POST["comment"])))
        OR (($_GET["room"] == "laptops") AND (empty($_POST["number"])) AND 
        (count($_POST['add1']) > 0)) 
        OR (($_GET["room"] == "laptops") AND (empty($_POST["number"])) AND 
        (count($_POST['add2']) > 0))) {
            echo '<h3>Error</h3><p>I\'m sorry, but your request could not be 
                completed because of the following:<ol>';

            if (((!count($_POST['add1']) > 0) AND (!count($_POST['del1']) > 0)
                AND (!count($_POST['add2']) > 0) AND (!count($_POST['del2']) > 0))) {
                    echo '<li>You didn\'t <b>select</b> any periods</li>';
            }

            if  (((count($_POST['add1']) > 0) AND (empty($_POST["comment"]))) 
                OR ((count($_POST['add2']) > 0) AND (empty($_POST["comment"])))) {
                    echo '<li>You must enter a <b>description</b> if you are booking a room</li>';
            }

            if (($_GET["room"] == "laptops") AND (empty($_POST["number"]))) {
                echo '<li>You must enter the <b>number</b> of laptops required</li>';
            }

            else // You shouldn't see this, but just in case
                echo '<li>Unexpected Error</li>';

            echo '</ol>';
    }

    else {
        // Create directoires if needed
        if (!file_exists($bookdir1))
            mkdir($bookdir1, 0777);
        if (!file_exists($bookdir2))
            mkdir($bookdir2, 0777);
        if (!file_exists($roomdir1))
            mkdir($roomdir1, 0777);
        if (!file_exists($roomdir2))
            mkdir($roomdir2, 0777);

        // Create and delete bookings as requested
        if (count($_POST['add1']) > 0) {
            foreach ($_POST['add1'] as $addnum) {
                if ($_GET["room"] == "laptops") {
                    if ((file_exists($roomdir1 . '/' . $addnum . $ext)) 
                        AND (!file_exists($roomdir1 . '/' . $addnum . '-1' . $ext))) {
                            file_put_contents($roomdir1 . '/' . $addnum . '-1' . $ext,
                                '<?php $owner="' . $_COOKIE["user"] . '"; $date="'
                                 . date("Ymd-His") . '"; $number="x' . $_POST["number"] 
                                 . '"; ?>' . $_POST["comment"] . ' ' . $_COOKIE["user"]);
                    }

                    elseif ((file_exists($roomdir1 . '/' . $addnum . '-1' . $ext)) 
                        AND (!file_exists($roomdir1 . '/' . $addnum . '-2' . $ext)) 
                        AND (file_exists($roomdir1 . '/' . $addnum . $ext))) {
                            file_put_contents($roomdir1 . '/' . $addnum . '-2' . $ext,
                                '<?php $owner="' . $_COOKIE["user"] . '"; $date="' 
                                . date("Ymd-His") . '"; $number="x' . $_POST["number"] 
                                . '"; ?>' . $_POST["comment"] . ' ' . $_COOKIE["user"]);
                    }

                    elseif ((file_exists($roomdir1 . '/' . $addnum . '-2' . $ext)) 
                        AND (!file_exists($roomdir1 . '/' . $addnum . '-3' . $ext)) 
                        AND (file_exists($roomdir1 . '/' . $addnum . $ext))) {
                            file_put_contents($roomdir1 . '/' . $addnum . '-3' . $ext,
                                '<?php $owner="' . $_COOKIE["user"] . '"; $date="' 
                                . date("Ymd-His") . '"; $number="x' . $_POST["number"] 
                                . '"; ?>' . $_POST["comment"] . ' ' . $_COOKIE["user"]);
                    }

                    else {
                        file_put_contents($roomdir1 . '/' . $addnum . $ext,
                            '<?php $owner="' . $_COOKIE["user"] . '"; $date="' 
                            . date("Ymd-His") . '"; $number="x' . $_POST["number"] 
                            . '"; ?>' . $_POST["comment"] . ' ' . $_COOKIE["user"]);
                    }
                }

                elseif (file_exists($roomdir1 . '/' . $addnum . $ext)) {
                    if ($duplicatenum == 0) {
                        echo $duplicate;
                        $duplicatenum++;
                        $error++;
                    }
                }

                else {
                    file_put_contents($roomdir1 . '/' . $addnum . $ext,
                        '<?php $owner="' . $_COOKIE["user"] . '"; $date="' 
                        . date("Ymd-His") . '"; $number="x' . $_POST["number"] 
                        . '"; ?>' . $_POST["comment"] . ' ' . $_COOKIE["user"]);
                }
            }
        }

        if (count($_POST['add2']) > 0) {
            foreach ($_POST['add2'] as $addnum) {
                if ($_GET["room"] == "laptops") {
                    if ((file_exists($roomdir2 . '/' . $addnum . $ext)) 
                        AND (!file_exists($roomdir2 . '/' . $addnum . '-1' . $ext))) {
                            file_put_contents($roomdir2 . '/' . $addnum . '-1' . $ext,
                                '<?php $owner="' . $_COOKIE["user"] . '"; $date="' . 
                                date("Ymd-His") . '"; $number="x' . $_POST["number"] 
                                . '"; ?>' . $_POST["comment"] . ' ' . $_COOKIE["user"]);
                    }

                    elseif ((file_exists($roomdir2 . '/' . $addnum . '-1' . $ext)) 
                        AND (!file_exists($roomdir2 . '/' . $addnum . '-2' . $ext)) 
                        AND (file_exists($roomdir2 . '/' . $addnum . $ext))) {
                            file_put_contents($roomdir2 . '/' . $addnum . '-2' . $ext,
                                '<?php $owner="' . $_COOKIE["user"] . '"; $date="' 
                                . date("Ymd-His") . '"; $number="x' . $_POST["number"] 
                                . '"; ?>' . $_POST["comment"] . ' ' . $_COOKIE["user"]);
                    } 

                    elseif ((file_exists($roomdir2 . '/' . $addnum . '-2' . $ext)) 
                        AND (!file_exists($roomdir2 . '/' . $addnum . '-3' . $ext)) 
                        AND (file_exists($roomdir2 . '/' . $addnum . $ext))) {
                            file_put_contents($roomdir2 . '/' . $addnum . '-3' . $ext,
                                '<?php $owner="' . $_COOKIE["user"] . '"; $date="' 
                                . date("Ymd-His") . '"; $number="x' . $_POST["number"] 
                                . '"; ?>' . $_POST["comment"] . ' ' . $_COOKIE["user"]);
                    }

                    else {
                        file_put_contents($roomdir2 . '/' . $addnum . $ext,
                            '<?php $owner="' . $_COOKIE["user"] . '"; $date="' 
                            . date("Ymd-His") . '"; $number="x' . $_POST["number"] 
                            . '"; ?>' . $_POST["comment"] . ' ' . $_COOKIE["user"]);
                    }
                }

                elseif (file_exists($roomdir2 . '/' . $addnum . $ext)) {
                    if ($duplicatenum == 0) {
                        echo $duplicate;
                        $duplicatenum++;
                        $error++;
                    }
                }

                else {
                    file_put_contents($roomdir2 . '/' . $addnum . $ext,
                        '<?php $owner="' . $_COOKIE["user"] . '"; $date="' 
                        . date("Ymd-His") . '"; $number="x' . $_POST["number"] 
                        . '"; ?>' . $_POST["comment"] . ' ' . $_COOKIE["user"]);
                }
            }
        }

        if (count($_POST['del1']) > 0) {
            foreach ($_POST['del1'] as $delnum) {
                rename($roomdir1 . '/' . $delnum . $ext, $roomdir1 . '/' 
                    . $delnum . '-' . date("Ymd-His") . '-' . $ext);
            }
        }

        if (count($_POST['del2']) > 0) {
            foreach ($_POST['del2'] as $delnum) {
                rename($roomdir2 . '/' . $delnum . $ext, $roomdir2 . '/' 
                    . $delnum . '-' . date("Ymd-His") . '-' . $ext);
            }
        }

        if ($error == 0) {
        // Present feedback
        // TODO Actually check files have been written/deleted with something
        // like:
        //if ((count($_POST['add']) > 0) AND (file_exists($roomdir . '/' .
        //$addnum . $ext))) etc...
            echo $savedmsg;
        }
    }
    // echo $error; // This will show how many errors were encountered - used for debugging
}

else
    echo $howtomsg;

echo '                      </td>
                        <td rowspan="2" class="user">';

if (isset($_COOKIE["user"])) {
    echo "Logged&nbsp;in&nbsp;as&nbsp;<b>" . $_COOKIE["user"] . "</b>";
    echo '                                <form action="login.php" method="POST">
                                    <input type="submit" name="logout" 
                                        value="Log Out">
                                </form>';
}

echo '                      </td>
                    </tr>
                    <tr>
                        <td>
                            <h1>' . $name . '</h1>
                        </td>
                    </tr>
                </table>
';

// Numbers to increment for unique values
$numtitle=0;
$numday=0;
$numcell1=0;
$numcell2=0;
$linknum=1;

if (empty($_GET["room"])) {
    echo $welcome;
}

echo '                <table class=rooms>
                    <tr>
                        <td>';

// Create resource links
echo '                    <table>
                    <tr>
                        <td class=roomtitles>Resources:</td>
                    </tr>
                    <tr>
                        <td>';

while($linknum<=$roomnum) {
    echo '<a ';

    if ($_GET["room"] == $room[$linknum])
        echo 'class="selected" ';
    else
        echo 'class="links" ';
    
    echo 'href="index.php?room=' . $room[$linknum] . '">' . ucwords(strtolower($room[$linknum])) 
        . '</a>';

    $linknum++;
}

echo '                        </td>
                    </tr>
                </table>
            </td>
            <td>&nbsp;</td>
            <td class=bottom>';

if (in_array($_GET["room"],$room)) {
    echo '<form action="index.php?room=' . $_GET['room'] . '" method="POST">' 
        . $commentmsg . '<input class="field" name="comment" maxlength="' .
        $commentmax . '" value="">';

    if ($_GET["room"] == 'laptops') {
        echo $numbermsg . '<input class="field" name="number" maxlength="' .
            $numbermax . '" value="">';
    }

    echo '<input type="submit" name="save" value="Save Changes">';
    echo '<input type="hidden" name="room" value="' . $_GET["room"] . '">';
}

echo '            </td>
        </tr>
    </table>';

// Development code to show rooms with free periods
if (empty($_GET["room"])) {
    //What rooms are free today?
    //echo '<p id="free"><b>What\'s free today?</b></p>
    //include_once('free.php');
}

// Make sure a valid room has been specified
if (in_array($_GET["room"],$room)) {
    // This is a new form to refresh the page cleanly (just a button), but if I
    // end the form after the button, it seems to end the main form too...
    echo '<p>
            <form action="index.php?room=' . $_GET['room'] . '" method="POST">
                <input type="submit" value="Refresh Page">&nbsp;&nbsp;
    
            <a href="print.php?room=' . $_GET['room'] . '">Printer Friendly
                Version</a></p>';

    // Show the weeks (tables)
    include_once('week1.php');
    include_once('week2.php');

    echo '</form>';
}

// Error displayed if invalid room has been specified
elseif (!empty($_GET["room"])) {
    echo $invalidmsg;
}

// Do we need an "else" here to print the $invalidmsg?

echo '              </td>
        </tr>
        <tr>
            <td class="foot">
                ' . $footer . '
            </td>
        </tr>
    </table>
</body>
</html>';
