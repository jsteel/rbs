<?php

// Cell contents for week 2

// For debugging
//echo '<font size="1px">Cell #' . $dayperiod2 . '</font>';

if ($_GET["room"] !== "laptops") {
    if (file_exists($ttdir2.$_GET['room'].'/'.$dayperiod2.$ext)) {
        echo '<div id="timetabled">';
        include_once($ttdir2.$_GET['room'].'/'.$dayperiod2.$ext);
        echo '</div>';
    }

    elseif (file_exists($bookdir2.$_GET['room'].'/'.$file.$dayperiod2.$ext)) {
        include_once($bookdir2.$_GET['room'].'/'.$file.$dayperiod2.$ext);
            if ((($_COOKIE["user"]) == $owner) OR $adminusers) {
                    echo '<input type="checkbox" name="del2[]" value="' 
                        .$file.$dayperiod2.'" />';
            }
    }
    else {
        echo '<input type="checkbox" name="add2[]" value="'
            .$file.$dayperiod2.'" />';
    }
}

// Laptops (multiple booking resource)
else {
    if (file_exists($ttdir2.$_GET['room'].'/'.$dayperiod2.$ext)) {
        echo '<div id="timetabled">';
        include_once($ttdir2.$_GET['room'].'/'.$dayperiod2.$ext);
        echo '</div>';
    }

    if ((file_exists($bookdir2.$_GET['room'].'/'.$file.$dayperiod2.$ext)) 
        OR (file_exists($bookdir2.$_GET['room'].'/'.$file.$dayperiod2.'-1'.$ext)) 
        OR (file_exists($bookdir2.$_GET['room'].'/'.$file.$dayperiod2.'-2'.$ext))
        OR (file_exists($bookdir2.$_GET['room'].'/'.$file.$dayperiod2.'-3'.$ext))) {
            if (file_exists($bookdir2.$_GET['room'].'/'.$file.$dayperiod2.$ext)) {
                include_once($bookdir2.$_GET['room'].'/'.$file.$dayperiod2.$ext);
                echo ' ' . $number;
                    if ((($_COOKIE["user"]) == $owner) OR $adminusers) {
                            echo '<input type="checkbox" name="del2[]" value="'
                                .$file.$dayperiod2.'" />';
                    }
            }
    }

    if ((file_exists($bookdir2.$_GET['room'].'/'.$file.$dayperiod2.'-1'.$ext))) {
        if (file_exists($bookdir2.$_GET['room'].'/'.$file.$dayperiod2.$ext)) {
            echo '<br>';
            include_once($bookdir2.$_GET['room'].'/'.$file.$dayperiod2.'-1'.$ext);
        }

        echo ' ' . $number;

        if ((($_COOKIE["user"]) == $owner) OR $adminusers) {
            echo '<input type="checkbox" name="del2[]" value="'
                .$file.$dayperiod2.'-1" />';
        }
    }
    
    if (file_exists($bookdir2.$_GET['room'].'/'.$file.$dayperiod2.'-2'.$ext)) {
        if ((file_exists($bookdir2.$_GET['room'].'/'.$file.$dayperiod2.'-1'.$ext)) 
            OR (file_exists($bookdir2.$_GET['room'].'/'.$file.$dayperiod2.$ext))) {
                echo '<br>';
                include_once($bookdir2.$_GET['room'].'/'.$file.$dayperiod2.'-2'.$ext);
        }

        echo ' ' . $number;

        if ((($_COOKIE["user"]) == $owner) OR $adminusers) {
            echo '<input type="checkbox" name="del2[]" value="'
                .$file.$dayperiod2.'-2" />';
        }
    }

    if ((file_exists($bookdir2.$_GET['room'].'/'.$file.$dayperiod2.'-3'.$ext))) {
        if ((file_exists($bookdir2.$_GET['room'].'/'.$file.$dayperiod2.'-2'.$ext)) 
            OR (file_exists($bookdir2.$_GET['room'].'/'.$file.$dayperiod2.'-1'.$ext)) 
            OR (file_exists($bookdir2.$_GET['room'].'/'.$file.$dayperiod2.$ext))) {
                echo '<br>';
                include_once($bookdir2.$_GET['room'].'/'.$file.$dayperiod2.'-3'.$ext);
        }

        echo ' ' . $number;

        if ((($_COOKIE["user"]) == $owner) OR $adminusers) {
            echo '<input type="checkbox" name="del2[]" value="'
                .$file.$dayperiod2.'-3" />';
        }
    }

    if ((!file_exists($bookdir2.$_GET['room'].'/'.$file.$dayperiod2.$ext))
        OR (!file_exists($bookdir2.$_GET['room'].'/'.$file.$dayperiod2.'-1'.$ext))
        OR (!file_exists($bookdir2.$_GET['room'].'/'.$file.$dayperiod2.'-2'.$ext))
        OR (!file_exists($bookdir2.$_GET['room'].'/'.$file.$dayperiod2.'-3'.$ext))) {
            echo '<br><input type="checkbox" name="add2[]" value="'
                .$file.$dayperiod2.'" />';
    }

    else
        echo '<input type="checkbox" name="add2[]" value="'.$file.$dayperiod2.'" />';
}
