<?php

ob_start();
include_once('config.php');

// This forces https, which is highly recommended
if ($_SERVER['SERVER_PORT'] != 443) {
    header("Location: https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
}

echo '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
	    <link rel="stylesheet" type="text/css" href="style.css">
		<title>' . $name . '</title>
        <script type="text/javascript">
            function formfocus() {
                document.getElementById(\'username\').focus();
            }
            window.onload = formfocus;
        </script>
    </head>
<body>
    <table class="page">
        <tr>
            <td>
                <form action="login.php" method="POST">
                    <table border=0>
                        <tr>
                            <td class="logologin" colspan="2">
                                <img src="' . $logo . '" alt="Logo">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <h2>' . $name . '</h2>
                            </td>
                        </tr>
                        <tr>
                            <td>Username:</td>
                            <td align="right">
                                <input class="field" type="text" name="username" 
                                    maxlength="20" id="username" 
                                    value="' . $_POST['username'] . '">
                            </td>
                        </tr>
                        <tr>
                            <td>Password:</td>
                            <td align="right"><input class="field" type="password" 
                                name="password" maxlength="20">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="right">
                                <input type="submit" value="Log In">
                            </td>
                        </tr>
                    </table>
                </form>
            ';

// LDAP details
$dn = $_POST["username"];
$pass = $_POST["password"];
$filter = 'sAMAccountName=' . $_POST["username"] . '';

// Log out (delete cookie)
if (isset($_POST["logout"])) {
    setcookie("user", "", time()-3600, "/");
    echo $logoutmsg;
}

// Log in
if (isset($_POST["username"])) {
    // This hides unwanted errors when authentication fails
    ini_set( "display_errors", 0);

    // The credentials entered simply query AD; if it succeeds, the
    // credentials given are correct
    $ds=ldap_connect($host, $port);
    ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION,3);
    ldap_set_option($ds, LDAP_OPT_REFERRALS,0);
    $r=ldap_bind($ds, $dn, $pass);
    $sr=ldap_search($ds, $base, "($filter)");
    $info = ldap_get_entries($ds, $sr);
    ldap_close($ds);

    // Set cookie and redirect to index.php
    if ($info["count"] > 0) {
        setcookie("user", $_POST["username"], time()+36000, "/");
        header('Location: index.php');
    }

    // No results? Show login error
    if (empty($info["count"]))
        echo $loginerror;
}

echo '</td>
        </tr>
        <tr>
            <td class="foot">
                ' . $footer . '
            </td>
        </tr>
    </table>
</body>
</html>';
