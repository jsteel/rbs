#!/bin/bash
# This should be run through cron weekly to backup/delete old bookings

DIR=/srv/http/rbs
mkdir -p "$DIR"/old-bookings

# Calculate the week number
WEEK=$[ $(date +"%W" | sed 's/0*//') % 2 ]

# Use scripts/which-week.sh to find out which week it is now. An even number
# will match the first "mv" below, and an odd the second.
# This may need to be changed again (switch "week1" and "week2") if the system
# is "paused" for a length of time, or in a new year.
if [ $WEEK -eq "0" ]; then
    mv "$DIR"/bookings/week2/ "$DIR"/old-bookings/$(date +"%F")
else
    mv "$DIR"/bookings/week1/ "$DIR"/old-bookings/$(date +"%F")
fi

exit
