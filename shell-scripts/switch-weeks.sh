#!/bin/bash
# This should be run through cron weekly to "rotate" the two weeks

DIR=/srv/http/rbs

cp "$DIR"/week1.php "$DIR"/week3.php && cp "$DIR"/week2.php "$DIR"/week1.php && \
mv "$DIR"/week3.php "$DIR"/week2.php

exit
