#!/bin/bash
# Find out which week it is (for use with scripts/delete-bookings.sh)

echo "It is week $(date +%W | sed 's/0*//')"

exit
